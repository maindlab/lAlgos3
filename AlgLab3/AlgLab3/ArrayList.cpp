#include "ArrayList.h"


template <class T>
ArrayList<T>::ArrayList()
{
	size_of_my_array =10;
	resizer = 5;
	count = 0;
	arrayPtr =(*T)calloc(size_of_my_array,sizeof(T));
}

template <class T>
ArrayList<T>::ArrayList(int size_send) 
{
	size_of_my_array = size_send;
	resizer = 5;
	count = 0;
	arrayPtr =(*T)calloc(size_of_my_array,sizeof(T));
}

template <class T>
ArrayList<T>::~ArrayList()
{
	free(arrayPtr);
}
template <class T>
void ArrayList<T>::resize() {
	realloc(arrayPtr, size_of_my_array);
}
template<class T>
bool ArrayList<T>::add(T element) {
	if (count == size_of_my_array) {
		// почему недопустимо использовать +=
		size_of_my_array = size_of_my_array+ resizer;
		resize();
	}
	
	arrayPtr[count] = element;
	count++;
}
template<class T>
bool ArrayList<T>::deleteElement(int pos) {
	if (pos < 0)
		throw "invalide size of array";
	for (int i = pos; i < count; i++) {
		arrayPtr[i - 1] = arrayPtr[i];
	}
	if (size_of_my_array - count > resizer) {
		size_of_my_array = size_of_my_array -resizer;
		resize();
	}
	count--;
	return true;
}
template<class T>
T ArrayList<T>::get(int i) {
	if (i < 0)
		throw "invalide size of array";
	return arrayPtr[i];
}
template<class T>
unsigned int ArrayList<T>::getCount() {
	return count;
}
template<class T>
unsigned int ArrayList<T>::getSize() {
	return size_of_my_array;
}
template<class T>
bool ArrayList<T>::set(int i, T value) {
	if (i < 0)
		throw "invalide size of array";
	arrayPtr[i] = value;
	return true;
}
template<class T>
bool ArrayList<T>::setResize(int resize) {
	if (resize < 0)
		throw "invalide value of resize";
	resizer = resize;
	return true;
}
template class ArrayList<std::string>;

