#pragma once
#include <cstdlib>
#include <string>
using namespace std;

template <class T>
class ArrayList
{
private:
	//������� ������ ������
	 int size_of_my_array;
	// ������� ���-�� ��������� ������
	 int count;
	// ����� ������������ ��������� ��������� ����������� �������
	int resizer;
	//������� ������
	T *arrayPtr;
	void resize();
public:
	// ��������� ����������� �� 10 ���������
	ArrayList();
	//����������� �� ��������� ���-�� ���������
	ArrayList(int);
	~ArrayList();
	//��������� ������� � ����� �����
	bool add(T);
	//������� ��������� �������
	bool deleteElement(int);
	// �������� ��������� �������
	T get(int);
	// ���������� ���-�� ���������
	unsigned int getCount();
	// ���������� ������ �����
	unsigned int getSize();
	// ������������� ���������� ������� � ��������� �������
	bool set(int, T);
	// ����� �� ������������
	bool setResize(int);
};

